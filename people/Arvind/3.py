# Material Questions
#
# - which institute was the lecture being recorded in?
# - * name one technique mentioned in the lecture?
# - name one demo program shown in the lecture?
# - what's his opinion on attending class?
# - name another demo program shown in the lecture?
# - * name the other technique mentioned in the lecture?
# - when did the professor die?
# - please give an example of an intelligent task?
# - name one component of intelligence?
# - * what kind of intelligence does sophia display?
# - name another component of intelligence?
# - * what kind of intelligence did the FB bot display?
# - how can AI play a role in classic engineering? For example antenna design?
# - can AI be dangerous to humans?
#
# - lipsa. please explain what that cache thing did
#
# ========================================
# Class Discussion Flow
#
# - online etiquete.
# - questions
# - the big areas: planning/perception/reasoning/causality

# - notes
#    - aiml
# - assignment discussion
# - discussion
#
# ========================================
# 1. Copy an assignment file from this folder to the `people/<your name>` folder
# 2. Edit the `people/<your name>/<assignment number>.py` file and write your solution in it
# 3. Create a merge request to submit
# 4. Ensure that
#       - tests pass for your solution in the merge request
#       - you have setup your gitlab fork to auto-mirror the class
#
#
# =========================================
# The task is simple enough. We all know how to cross a road. You need to write
# a program which can cross a road.

# To complete this assignment please fill up the function given below.
# the function takes two arguments: "left_side", "right_side"
# these are two lists containing what you see when you see left and right while
# standing on the road. For example:

# in a one lane road, if you look left and right you might see the following

# [[None, None, 'car', None, None]]  [[None, None, None]]
#                                  ↑
#                             <my position>

# The 'left' list is a list of lists and contains either None or a string
# describing what is there in that location. Similarly you have a 'right' list.
# Your function must return True if it is possible to cross the road, given
# this situation. Otherwise your function must return False

# You can assume that traffic is moving right to left.


def assignment_3(left_side, right_side):
    if right_side[0][0] is None:
        return True
    else:
        return False
