import gym
import random

env = gym.make("FrozenLake-v0")
env.reset()
env.render()
reward = 0.00

forbidden_state = [5, 7, 11, 12]

actions = {"L": 0, "D": 1, "R": 2, "U": 3}
counter = 0
bool = True
while bool:
    counter = counter + 1
    win_seq = [
        random.choice(["L", "D", "R"]),
        random.choice(["L", "D", "R"]),
        random.choice(["L", "D"]),
        random.choice(["L", "D", "R", "U"]),
    ]
    for a in win_seq:
        new_state, reward, done, info = env.step(actions[a])
        print()
        env.render()
        print("Reward: {:.2f}".format(reward))
        if new_state in forbidden_state:
            env.reset()
            break
        if new_state == 15:
            bool = False
            break
print("no.of attempts", counter)
print("the winning sequence", win_seq)
